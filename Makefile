MAIN='main'
OPT=-shell-escape

all: compile clean

compile:
	mkdir -p out.o
	lualatex ${OPT} ${MAIN}.tex

clean:
	rm -rf *.aux *.log *.out *.txt
	rm -rf *.auxlock *.bcf *.blg *.bbl *-blx.bib *.run.xml
	rm -rf *.toc *.snm *.nav

squeaky:
	rm -rf out.o/* 
	rm -f cdc-figure*.*
